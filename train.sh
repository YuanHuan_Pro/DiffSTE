#!/opt/conda/bin bash
source /opt/conda/bin/activate base
conda init bash
conda activate diffste
export PYTHONPATH=/zhangye/code/DiffSTE:$PYTHONPATH
cd /zhangye/code/DiffSTE
save_model_dir=/zhangye/code/DiffSTE/logs/All_city_clean_ch_char_13052_202407/
python train.py --base_logdir=$save_model_dir
