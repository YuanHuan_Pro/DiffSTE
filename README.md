## 【介绍】
这里是整个 DIFFSTE 的代码仓库，全部代码以车牌样本生成为样例编写，同时包含数据预处理，训练，推理等脚本

## 【背景】
1. 项目背景：车牌识别OCR任务，识别车牌号码
2. 项目难点：
	- 不同国家、不同地区车牌样式多样，车牌颜色、样式、风格
	- 车牌不同模糊程度、不同明暗程度
	- 不同焦距、不同分辨率、不同镜头、不同设备，车牌清晰度不一样
	- 获得一个准确率达标的车牌识别OCR模型，需要大量的数据支撑，耗时耗力
3. 利用样本生成技术可以生成风格多样且真实数据，提升车牌识别算法性能

## 【技术方案】

1. 模型结构
	- <img src="assets/dualencoder.png" width="600">
2. 模型思路介绍
	- 提出字符编码器分支。在 SD 的条件分支中，加入字符编码信息。其本质是一个简单的字符Embedding，可以引导 SD 扩散过程生成正确拼写的字符
	- 通过设计准确描述当前的生成字符文本风格的文本输入作为文本条件引导，其文本编码器继承于原始 SD
	- 将两者结合，字符编码器信息和文本编码器信息共同作为条件，引导最终结果生成
3. 模型输入介绍
	- 描述所需目标文本的文本指令，指定颜色和字体
	- masked image 一个被周围类似风格的场景文本包围的遮蔽区域，指示应该在图像中生成文本的位置
4. 车牌项目应用
	- 现状：车牌的文本风格相对多，且杂，且无法用精确语言描述
	- 方案：不依赖文本风格描述语句进行引导生成的，文本编码器的文本风格设置为“空”。车牌生成任务仅仅只是根据车牌中其他背景区域（其他字符风格）信息进行当前车牌风格的生成

## 【代码及预训练模型】
1. 代码：[https://gitlab.com/YuanHuan_Pro/DiffSTE](https://gitlab.com/YuanHuan_Pro/DiffSTE)，分支：DIffste_Common_Project，代码仍在调试中，暂未完全开发到在公司内网中，需要请联系：袁欢
2. 预训练模型：
	- [预训练模型](https://drive.google.com/file/d/1fc0RKGWo6MPSJIZNIA_UweTOPai64S9f/view?usp=share_link)
    - 预训练模型放在对应路径下：./checkpoints/chartokenizer/

## 【环境部署】
1. 地址：
	- 重庆集群 [https://10.30.25.10:32206/](https://10.30.25.10:32206/)
	- GPU：推荐 Tesla V100-PCIE-32G，两张显卡，24G 显存显卡显存不足无法训练
	- <img src="img/dockfile.png" width="400">
	
## 【数据准备】
1. 数据挑选：
	- 目的：挑选少量但风格样式多样的数据，参与模型训练和推理，用于生成风格多样的样本
	- 思想：城市均衡、字符数量均衡、字符字体均衡、颜色均衡、模糊程度均衡、光照程度均衡：为了保证生成车牌效果，覆盖每个城市，34 个字符数量均衡（数字+英文），字符字体均衡、颜色均衡
	- 注：为保证不同风格字符生成效果，每种风格车牌，单独挑选数据，训练 DIFFSTE 生成模型（举个例子：国内车牌，绿牌较蓝牌黄牌字体是不一样的，因此单独构建绿牌生成模型）
	- 注：挑选数据保证车牌号码标注正确，否则后续会产生大量错误的生成样本
2. 数据预处理：
	- 运行脚本 dataset_pretreatment/dataset_resize_padding.py, resize & padding 到模型训练尺寸, 车牌统一缩放到（256，128）的大小, 同时 padding 车牌到 DIFFSTE 生成模型数据（256，256）大小
	- 注：图像大小为（256，256），两侧黑框为 Padding 结果
3. 数据字符标注：
	- 目的：DIFFSTE 模型字符替换方案，模型输入是 masked image，指示应该在图像中生成文本的位置，车牌任务中需要用 mask 指示需要被替换的字符
	- 当前不支持汉字生成，所以**汉字不需要标注**
	- 数据打标签采用VOC数据格式，示例如下：
	- <img src="img/label.png" width="800">

## 【训练&验证】
1. 配置文件修改：diffste/configs/config_charinpaint.yaml，修改对应的训练集和验证集路径
	```
	dataconfs:
	TextOCR:
		type: scene
		label_path: /zhangye/DiffSTE/ocr-dataset/diff_ste_green_big_dataset/validation/annotations
		image_dir: /zhangye/DiffSTE/ocr-dataset/diff_ste_green_big_dataset/validation/images
	```
2. 编写 train.sh：
	```
	#!/usr/bin/fish
	conda activate diffste
	cd /xxx/DiffSTE
	python train.py
	```
3. 模型训练：
	```
	cd /xxx/DiffSTE && fish ./train.sh
	```
4. 训练完毕后，模型保存位置：logs/charinpaint/xxxx/checkpoints/xxx.ckpt
5. 准备推理 mask
	- 运行脚本 scripts/dataset_plate/dataset_pretreatment/dataset_mask.py，根据标注文件，生成重绘区域 mask
	- <img src="img/data_sample.png" width="400">
6. 单张图片推理：	
	- 执行单张图像推理脚本
	```
	python generate.py --ckpt_path logs/charinpaint/xxxx/checkpoints/xxx.ckpt \
		--in_image  examples/my_sample.png \
		--in_mask examples/my_mask.png \
		--text 6
	```
7. 数据集并行推理：
	- 生成对应的每张卡的推理 replace_txt 文件
		- 例如：
			- 对应的 txt 文件存取的是每张图的路径和其对应的mask路径，以逗号隔开，例如：
			```
			img_path,mask_0_path,mask_1_path,mask_2_path,...
			```
	- 开始推理，运行推理脚本
	```
	python ./scripts/dataset_plate/infer/generate_10K_syn_images.py --ckpt_path /yuanhuan/code/demo/Image/sd/diffste/logs/charinpaint/2023-10-11T-03-33-08_train_exp/checkpoints/epoch=15-step=002304.ckpt \
		--out_dir /yuanhuan/data/image/RM_ANPR/original/us/DIFFSTE/diffste_Texas_665_scale_padding/5/ \
		--replace_txt /yuanhuan/data/image/RM_ANPR/training/DIFFSTE/us/original_Texas_665/train/mask_5.txt
	```

## 【实验结果展示】
- <img src="img/to_show_pic.png" width="800">


## 【存在问题】
1. 对数据要求高，需要人为标注 mask。目前的解决思路是训练检测模型，辅助标注
2. 部分字符生成效果差，可以通过严格筛选数据，控制分布解决