import os
import torch
from PIL import Image
from omegaconf import OmegaConf
from argparse import ArgumentParser
from torchvision.utils import make_grid
from pytorch_lightning import seed_everything
from torchvision.transforms import ToPILImage, ToTensor
from src.trainers import CharInpaintTrainer
from src.dataset import prepare_style_chars
from src.dataset.utils import prepare_npy_image_mask, normalize_image


def create_parser():
    parser = ArgumentParser()
    parser.add_argument("--seed", type=int, default=13)
    # parser.add_argument("--ckpt_path", type=str, default='/yuanhuan/model/image/sd/diffste/us_Texas/2023-10-11T-03-33-08_train_exp/checkpoints/epoch=15-step=002304.ckpt')
    # parser.add_argument("--in_image", type=str, default='/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/us/original_Texas_665/train/images/0000000000000000-230905-094047-095557-000006000060_094147_1.00_24.00_00010-00_none_single_RLV6082.jpg')
    # parser.add_argument("--in_mask", type=str, default='/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/us/original_Texas_665/train/masks/0000000000000000-230905-094047-095557-000006000060_094147_1.00_24.00_00010-00_none_single_RLV6082_mask_0_4.jpg')
    # parser.add_argument("--ckpt_path", type=str, default='/yuanhuan/model/image/sd/diffste/chile/2024-01-08T-03-16-00_train_exp/checkpoints/epoch=15-step=000496.ckpt')
    # parser.add_argument("--in_image", type=str, default='/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/chile/original_chile_20231202_20231203/train/images/0000000000000000-231201-090649-102744-000005005130_101355_3.00_49.00_00510-00_none_single_KC-TL^81.jpg')
    # parser.add_argument("--in_mask", type=str, default='/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/chile/original_chile_20231202_20231203/train/masks/0000000000000000-231201-090649-102744-000005005130_101355_3.00_49.00_00510-00_none_single_KC-TL^81_mask_8_4.jpg')
    # parser.add_argument("--ckpt_path", type=str, default='/yuanhuan/model/image/sd/diffste/eu_turkey/2024-03-13T-10-54-02_train_exp/checkpoints/epoch=15-step=002304.ckpt')
    # parser.add_argument("--in_image", type=str, default='/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/eu/turkey_2021_2023/train/images/0000000000000000-210311-134200-134645-000001004090.xml0000001077-01_none_single_34TFH44.jpg')
    # parser.add_argument("--in_mask", type=str, default='/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/eu/turkey_2021_2023/train/masks/0000000000000000-210311-134200-134645-000001004090.xml0000001077-01_none_single_34TFH44_mask_4_6.jpg')
    # parser.add_argument("--ckpt_path", type=str, default='/yuanhuan/model/image/sd/diffste/us_Al_US_network/2024-05-27T-12-18-49_train_exp/checkpoints/epoch=15-step=018464.ckpt')
    # parser.add_argument("--in_image", type=str, default='/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/us/Al_US_network_7631/train/images/Alabama_1DH5872.jpg')
    # parser.add_argument("--in_mask", type=str, default='/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/us/Al_US_network_7631/train/masks/Alabama_1DH5872_mask_1_0.jpg')
    parser.add_argument("--ckpt_path", type=str, default='/zhangye/code/DiffSTE/logs/charinpaint/2024-07-17T-04-44-12_train_exp/checkpoints/epoch=00-step=000420.ckpt')
    parser.add_argument("--in_image", type=str, default='/defaultShare/RM_ANPR/All_city_clean_13052_202407/train/images/0000000000000000-220331-083058-093016-000001233640_2936.49_3038.09-sn01660-00_green_single_湘KD00670.jpg')
    parser.add_argument("--in_mask", type=str, default='/defaultShare/RM_ANPR/All_city_clean_13052_202407/train/masks/0000000000000000-220331-083058-093016-000001233640_2936.49_3038.09-sn01660-00_green_single_湘KD00670_mask_湘_0.jpg')
    # parser.add_argument("--in_mask", type=str, default='/defaultShare/RM_ANPR/All_city_clean_13052_202407/train/masks/0000000000000000-220331-083058-093016-000001233640_2936.49_3038.09-sn01660-00_green_single_湘KD00670_mask_6_5.jpg')
    # parser.add_argument("--in_mask", type=str, default='/defaultShare/RM_ANPR/All_city_clean_13052_202407/train/masks/0000000000000000-220331-083058-093016-000001233640_2936.49_3038.09-sn01660-00_green_single_湘KD00670_mask_D_2.jpg')
    parser.add_argument("--save_color", type=str, default='')
    parser.add_argument("--out_dir", default="output_dir")
    # parser.add_argument("--text", type=str, default='3')
    # parser.add_argument("--text", type=str, default='B')
    # parser.add_argument("--text", type=str, default='鲁')
    # parser.add_argument("--text", type=str, default='川')
    # parser.add_argument("--text", type=str, default='渝')
    parser.add_argument("--text", type=str, default='津')
    # parser.add_argument("--text", type=str, default='湘')
    parser.add_argument("--font", type=str, default="")
    parser.add_argument("--color", type=str, default="")
    parser.add_argument("--instruction", type=str, default='')
    parser.add_argument("--num_inference_steps", default=30)
    parser.add_argument("--num_sample_per_image", default=3, type=int)
    parser.add_argument("--guidance_scale", default=7.5, type=float)
    parser.add_argument("--no_cuda", action="store_true")

    return parser

def main(opt):
    model = CharInpaintTrainer.load_from_checkpoint(opt.ckpt_path)
    device = "cpu" if opt.no_cuda else "cuda"
    model = model.to(device)

    image = Image.open(opt.in_image)
    mask = Image.open(opt.in_mask).convert("1")
    raw_image, mask, masked_image, mask_coordinate = prepare_npy_image_mask(
        image, mask
    )

    if opt.instruction != '':
        style = opt.instruction
        char = opt.text
    else:
        char = opt.text
        color = opt.color
        font = opt.font
        style = prepare_style_chars(char, [font, color])

    torch.manual_seed(opt.seed)
    batch = {
        "image": torch.from_numpy(raw_image).unsqueeze(0).to(device),
        "mask": torch.from_numpy(mask).unsqueeze(0).to(device),
        "masked_image": torch.from_numpy(masked_image).unsqueeze(0).to(device),
        "coordinate": [mask_coordinate],
        "chars": [char],
        "style": [style],
    }

    generation_kwargs = {
        "num_inference_steps": opt.num_inference_steps,
        "num_sample_per_image": opt.num_sample_per_image,
        "guidance_scale": opt.guidance_scale,
        "generator": torch.Generator(model.device).manual_seed(opt.seed)
    }

    with torch.no_grad():
        results = model.log_images(batch, generation_kwargs)
    os.makedirs(opt.out_dir, exist_ok=True)
    keys = results.keys()
    os.makedirs(f"{opt.out_dir}/{opt.save_color}", exist_ok=True)
    for i, k in enumerate(keys):
        img = torch.cat([
            ((batch["image"][i:i + 1].cpu()) / 2. + 0.5).clamp(0., 1.),
            ((batch["masked_image"][i:i + 1].cpu()) / 2. + 0.5).clamp(0., 1.),
            results[k]
        ])
        grid = make_grid(img, nrow=5, padding=1)
        ToPILImage()(grid).save(f"{opt.out_dir}/{opt.save_color}/{k}-grid09_epoch14.png")


if __name__ == "__main__":
    parser = create_parser()
    opt = parser.parse_args()
    main(opt)