import os


def create_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

def get_sub_filepaths(folder):
    paths = []
    for root, dirs, files in os.walk(folder):
        for name in files:
            path = os.path.join(root, name)
            paths.append(path)
    return paths

def get_sub_filepaths_suffix(folder, suffix='.wav'):
    paths = []
    for root, dirs, files in os.walk(folder):
        for name in files:
            if not name.endswith(suffix):
                continue
            path = os.path.join(root, name)
            paths.append(path)
    return paths

def get_sub_filepaths_prefix(folder, prefix='img'):
    paths = []
    for root, dirs, files in os.walk(folder):
        for name in files:
            if not str(name).startswith(prefix):
                continue
            path = os.path.join(root, name)
            paths.append(path)
    return paths