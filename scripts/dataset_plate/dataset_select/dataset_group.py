import argparse
import numpy as np
import os
import sys
import shutil
from tqdm import tqdm

sys.path.insert(0, '/yuanhuan/code/demo/Image/sd/diffste')
from scripts.dataset_plate.utils.folder_tools import *


def dataset_group(args):

    # mkdir
    create_folder(args.output_img_dir)

    # img list 
    img_list = os.listdir(args.input_img_dir)
    print("data len: ", len(img_list))

    for idx in tqdm(range(len(img_list))):

        img_name = img_list[idx].replace(".jpg", "")
        lpr_num_name = img_name.split('_')[-1]
        img_path = os.path.join(args.input_img_dir, img_list[idx])

        # 车牌长度区分 length
        lpr_length = len(lpr_num_name)

        # 车牌字母区分 letter
        lpr_letter_list = np.array([letter if letter in args.char_labels else "" for letter in lpr_num_name])
        lpr_letter_list = lpr_letter_list[lpr_letter_list!=""]
        lpr_letter_list = list(set(list(lpr_letter_list)))
        
        # 车牌数字区分 num
        lpr_num_list = np.array([letter if letter in args.num_labels else "" for letter in lpr_num_name])
        lpr_num_list = lpr_num_list[lpr_num_list!=""]
        lpr_num_list = list(set(list(lpr_num_list)))
        
        for letter_idx in range(len(lpr_letter_list)):
            output_img_path = os.path.join(args.output_img_dir, str(lpr_length), lpr_letter_list[letter_idx], img_list[idx])
            create_folder(os.path.dirname(output_img_path))
            shutil.copy(img_path, output_img_path)

        for num_idx in range(len(lpr_num_list)):
            output_img_path = os.path.join(args.output_img_dir, str(lpr_length), lpr_num_list[num_idx], img_list[idx])
            create_folder(os.path.dirname(output_img_path))
            shutil.copy(img_path, output_img_path)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--date_name', type=str, default="Texas_20230811") 
    parser.add_argument('--ocr_name', type=str, default="plate_us_202310") 
    parser.add_argument('--input_dir', type=str, default="/yuanhuan/data/image/RM_ANPR/training/") 
    args = parser.parse_args()

    args.input_img_dir = os.path.join(args.input_dir, args.ocr_name, args.date_name, "Images")
    args.output_img_dir = os.path.join(args.input_dir, args.ocr_name, "Images_group")

    print("gen ocr img.")
    print("date_name: {}".format(args.date_name))
    print("ocr_name: {}".format(args.ocr_name))
    print("input_dir: {}".format(args.input_dir))

    args.char_labels = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'O', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    args.num_labels = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']

    # 数据分组
    dataset_group(args)
