from argparse import ArgumentParser
import copy
import numpy as np
import os
from pytorch_lightning import seed_everything
from PIL import Image
import random
import torch
from torchvision.utils import make_grid
from torchvision.transforms import ToPILImage
# from tqdm import tqdm
from rich.progress import track

from src.trainers import CharInpaintTrainer
from src.dataset import prepare_style_chars


def create_parser():
    parser = ArgumentParser()
    # parser.add_argument("--ckpt_path", type=str, default='# /yuanhuan/model/image/sd/diffste/us_Texas/2023-10-11T-03-33-08_train_exp/checkpoints/epoch=15-step=002304.ckpt')
    # parser.add_argument("--ckpt_path", type=str, default='/yuanhuan/model/image/sd/diffste/chile/2024-01-08T-03-16-00_train_exp/checkpoints/epoch=15-step=000496.ckpt')
    # parser.add_argument("--ckpt_path", type=str, default='/yuanhuan/model/image/sd/diffste/eu_turkey/2024-03-13T-10-54-02_train_exp/checkpoints/epoch=15-step=002304.ckpt')
    # parser.add_argument("--ckpt_path", type=str, default='/yuanhuan/model/image/sd/diffste/us_Al_US_network/2024-05-27T-12-18-49_train_exp/checkpoints/epoch=15-step=018464.ckpt')
    parser.add_argument("--ckpt_path", type=str, default='/yuanhuan/model/image/sd/diffste/cn_All_city/2024-06-17T-08-42-44_train_exp/checkpoints/epoch=15-step=004448.ckpt')
    parser.add_argument("--in_image", type=str, default='')
    parser.add_argument("--in_mask", type=str, default='')
    # parser.add_argument("--out_dir", default='/yuanhuan/data/image/RM_ANPR/original/us/DIFFSTE/diffste_Texas_665_scale_padding/test/')
    # parser.add_argument("--out_dir", default='/yuanhuan/data/image/RM_ANPR/original/Chile/DIFFSTE/diffste_chile_20231202_20231203_scale_padding/4/')
    # parser.add_argument("--out_dir", default='/yuanhuan/data/image/RM_ANPR/original/eu/DIFFSTE/diffste_turkey_2021_2023_scale_padding/0/')
    # parser.add_argument("--out_dir", default='/yuanhuan/data/image/RM_ANPR/original/us/DIFFSTE/diffste_Al_US_network_7631_scale_padding/0/')
    parser.add_argument("--out_dir", default='/yuanhuan/data/image/RM_ANPR/original/cn/DIFFSTE/diffste_all_city')
    parser.add_argument("--text", type=str, default='')
    parser.add_argument("--font", type=str, default="")
    parser.add_argument("--color", type=str, default="")
    parser.add_argument("--instruction", type=str, default='')
    parser.add_argument("--num_inference_steps", default=30)
    parser.add_argument("--num_sample_per_image", default=1, type=int)
    parser.add_argument("--guidance_scale", default=7.5, type=float)
    parser.add_argument("--save_info", type=str, default='')
    parser.add_argument("--no_cuda", action="store_true")
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--infer_batch_size", default=8, type=int)
    # parser.add_argument("--replace_txt", default="/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/us/original_Texas_665/train/mask_6.txt", type=str)
    # parser.add_argument("--replace_txt", default="/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/chile/original_chile_20231202_20231203/train/test/mask_4.txt", type=str)
    # parser.add_argument("--replace_txt", default="/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/eu/turkey_2021_2023/train/test/mask_0.txt", type=str)
    # parser.add_argument("--replace_txt", default="/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/us/Al_US_network_7631/train/test/mask_0.txt", type=str)
    parser.add_argument("--replace_txt", default="/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/cn/All_city_8981/train/mask.txt", type=str)
    # parser.add_argument("--replace_step", default=100, type=int, help="每个模版替换次数")
    # parser.add_argument("--replace_time_per_step", default=3, type=int, help="每个模版每次替换几个字符")
    parser.add_argument("--replace_step", default=10, type=int, help="每个模版替换次数")
    parser.add_argument("--replace_time_per_step", default=1, type=int, help="每个模版每次替换几个字符")

    return parser


# 先加载模型，只加载一次就行了。
def load_model(opt):
    model = CharInpaintTrainer.load_from_checkpoint(opt.ckpt_path)
    device = "cpu" if opt.no_cuda else "cuda"
    model = model.to(device)

    return model


def prepare_npy_image_mask(image, mask):
    size = image.size[0]
    image = np.array(image.convert("RGB"), dtype=np.float32)
    image = image.transpose(2, 0, 1)
    image = image / 127.5 - 1.0
    mask = np.array(mask.convert("L"))
    mask = mask.astype(np.float32) / 255.0
    mask = mask[None]
    mask[mask < 0.5] = 0
    mask[mask >= 0.5] = 1
    masked_image = image * (mask < 0.5)

    nonzeros = mask[0].nonzero()  # (2, N)
    minx, maxx = min(nonzeros[0], default=0), max(nonzeros[0], default=size)
    miny, maxy = min(nonzeros[1], default=0), max(nonzeros[1], default=size)
    mask_coordinate = np.array((minx, maxx, miny, maxy), dtype=np.int16)
    return image, mask, masked_image, mask_coordinate


def do_inference(opt, img_path, cur_char, mask_path, save_name, model):
    # 先赋值
    opt.in_image = img_path
    opt.in_mask = mask_path
    opt.save_info = save_name
    opt.text = cur_char
    device = "cpu" if opt.no_cuda else "cuda"
    batch = {}
    batch_info = {
        "image": [],
        "mask": [],
        "masked_image": [],
        "coordinate": [],
        "chars": [],
        "style": [],
    }

    for i in range(len(img_path)):
        image = Image.open(opt.in_image[i])
        mask = Image.open(opt.in_mask[i]).convert("1")
        raw_image, mask, masked_image, mask_coordinate = prepare_npy_image_mask(
            image, mask
        )

        if opt.instruction != '':
            style = opt.instruction
            char = opt.text[i]
        else:
            char = opt.text[i]
            color = opt.color
            font = opt.font
            style = prepare_style_chars(char, [font, color])
        torch.manual_seed(opt.seed)
        batch_info['image'].append(torch.from_numpy(raw_image).unsqueeze(0).to(device))
        batch_info['mask'].append(torch.from_numpy(mask).unsqueeze(0).to(device))
        batch_info['masked_image'].append(torch.from_numpy(masked_image).unsqueeze(0).to(device))
        batch_info['coordinate'].append(mask_coordinate)
        batch_info['chars'].append(char)
        batch_info['style'].append(style)

    batch['image'] = torch.cat(batch_info['image'], dim=0)
    batch['mask'] = torch.cat(batch_info['mask'], dim=0)
    batch['masked_image'] = torch.cat(batch_info['masked_image'], dim=0)
    batch['coordinate'] = batch_info['coordinate']
    batch['chars'] = batch_info['chars']
    batch['style'] = batch_info['style']


    generation_kwargs = {
        "num_inference_steps": opt.num_inference_steps,
        "num_sample_per_image": opt.num_sample_per_image,
        "guidance_scale": opt.guidance_scale,
        "generator": torch.Generator(model.device).manual_seed(opt.seed)
    }

    with torch.no_grad():
        results = model.log_images(batch, generation_kwargs)
    os.makedirs(opt.out_dir, exist_ok=True)
    keys = results.keys()
    for i, k in enumerate(keys):
        img = results[k]
        grid = make_grid(img, nrow=1, padding=1)
        ToPILImage()(grid).save(f"{opt.out_dir}/{opt.save_info[i]}")


if __name__ == '__main__':
    parser = create_parser()
    opt = parser.parse_args()
    my_model = load_model(opt)

    seed_everything(opt.seed)

    # normak
    char_zh_labels_list = ["皖", "浙", "津", "鄂", "粤", "苏", "京", "辽", "陕", "沪", "桂", "吉", "闽", "黑", "鲁", "豫", "新", "川", "渝", "贵", "冀", "赣", "湘", "晋", "琼", "蒙", "青", "云", "宁", "甘", "藏", "港"]
    char_zh_sp_labels_list = ["警", "挂", "学"]
    char_labels_list = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    num_labels_list = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

    with open(opt.replace_txt, 'r', encoding='gbk') as f:

        # lines
        lines = f.readlines()

        # 读取 batch 行
        # for i in tqdm(range(0, len(lines), opt.infer_batch_size), desc='generate_images', total=len(lines)):
        for i in track(range(0, len(lines), opt.infer_batch_size), description='generate_images', total=len(lines)/opt.infer_batch_size):
            # line_batch_list
            line_batch_list = []
            cur_lines = [i + _ for _ in range(opt.infer_batch_size - 1, -1, -1)]
            remain_lines = copy.deepcopy(cur_lines)
            for cur_line in cur_lines:
                if cur_line >= len(lines):
                    remain_lines.remove(cur_line)
            for cur_line in remain_lines[::-1]:
                line_batch_list.append(lines[cur_line])

            # 每个模版替换 opt.replace_step 次数
            for step in range(opt.replace_step):
                
                # init 
                total_infer_times = opt.replace_time_per_step
                mask_path_batch_list = [''] * len(line_batch_list)
                pre_img_path_batch_list = [''] * len(line_batch_list)
                pre_label_batch_list = [''] * len(line_batch_list)
                for j in range(len(line_batch_list)):
                    img_path = line_batch_list[j].split(',')[0]
                    img_path = img_path.replace('\\', '/')
                    img_name = os.path.basename(img_path)
                    # 真实标签
                    ori_label = img_path.split('_')[-1][:-4].replace('-', '').replace('^', '')

                    mask_path_batch_list[j] = line_batch_list[j].split(',')[1:]
                    pre_img_path_batch_list[j] = img_path
                    pre_label_batch_list[j] = ori_label

                try:
                    while total_infer_times > 0:

                        batch_img_path_list = []
                        batch_cur_char_list = []
                        batch_mask_path_list = []
                        batch_save_name_list = []
                        batch_cur_label_list = []

                        # 准备 batch 数据
                        for j in range(len(line_batch_list)):
                            
                            # check
                            if len(mask_path_batch_list[j]) == 0:
                                continue
                            
                            # 替换一次
                            # 随机选择一个 mask
                            cur_mask_number = random.randint(0, len(mask_path_batch_list[j]) - 1)
                            mask_path = mask_path_batch_list[j][cur_mask_number].strip()
                            mask_name = os.path.basename(mask_path)[:-4]

                            # 替换位置&替换字符
                            replace_pos = int(mask_name.split("_")[-1])
                            replace_label = str(mask_name.split("_")[-2])

                            # 随机生成要替换的数字或者字符
                            # 策略：字符换字符，数字换数字
                            if replace_label in char_labels_list:
                                current_char = random.choice(char_labels_list)
                            elif replace_label in num_labels_list:
                                current_char = random.choice(num_labels_list)
                            elif replace_label in char_zh_labels_list:
                                current_char = random.choice(char_zh_labels_list)
                            elif replace_label in char_zh_sp_labels_list:
                                current_char = random.choice(char_zh_sp_labels_list)
                            else:
                                raise Exception()

                            current_label = list(pre_label_batch_list[j])
                            current_label[replace_pos] = current_char  # 替换
                            current_label = ''.join(current_label)  # 生成新的标签

                            # 带有标签的新名字
                            save_name = mask_name + f"_iter_{step}_generated_{current_label}.jpg"
                            batch_img_path_list.append(pre_img_path_batch_list[j])
                            batch_cur_char_list.append(current_char)
                            batch_mask_path_list.append(mask_path)
                            batch_save_name_list.append(save_name)
                            batch_cur_label_list.append(current_label)
                            mask_path_batch_list[j].remove(mask_path_batch_list[j][cur_mask_number])

                                    
                        do_inference(opt, batch_img_path_list, batch_cur_char_list, batch_mask_path_list, batch_save_name_list, my_model)
                        pre_img_path_batch_list = [os.path.join(opt.out_dir, _) for _ in batch_save_name_list]
                        pre_label_batch_list = batch_cur_label_list
                        total_infer_times -= 1

                except:
                    continue