import argparse
import os
import sys
import shutil

sys.path.insert(0, '/yuanhuan/code/demo/Image/sd/diffste')
from scripts.dataset_plate.utils.folder_tools import *


def dataset_path(args):

    with open(args.trainval_file, "r") as f:
        for line in f: 
            img_path = line.strip()
            img_name = os.path.basename(img_path)
            xml_path = os.path.join(os.path.dirname(img_path), img_name.replace(".jpg", ".xml"))
            output_img_path = os.path.join(args.output_dir, args.trainval_subdir_name, args.img_subdir_name, img_name)
            output_xml_path = os.path.join(args.output_dir, args.trainval_subdir_name, args.xml_subdir_name, img_name.replace(".jpg", ".xml"))    

            # mkdir
            create_folder(os.path.dirname(output_img_path))
            create_folder(os.path.dirname(output_xml_path))

            shutil.copy(img_path, output_img_path)
            shutil.copy(xml_path, output_xml_path)

    with open(args.test_file, "r") as f:
        for line in f: 
            img_path = line.strip()
            img_name = os.path.basename(img_path)
            xml_path = os.path.join(os.path.dirname(img_path), img_name.replace(".jpg", ".xml"))
            output_img_path = os.path.join(args.output_dir, args.test_subdir_name, args.img_subdir_name, img_name)
            output_xml_path = os.path.join(args.output_dir, args.test_subdir_name, args.xml_subdir_name, img_name.replace(".jpg", ".xml"))    

            # mkdir
            create_folder(os.path.dirname(output_img_path))
            create_folder(os.path.dirname(output_xml_path))

            shutil.copy(img_path, output_img_path)
            shutil.copy(xml_path, output_xml_path)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_dir', type=str, default="/yuanhuan/data/image/RM_ANPR/original/us/DIFFSTE/original_Texas_665_scale_padding") 
    parser.add_argument('--output_dir', type=str, default="/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/us/original_Texas_665") 
    args = parser.parse_args()

    print("data path.")

    args.img_subdir_name = "images"
    args.xml_subdir_name = "annotations"
    args.trainval_subdir_name = "train"
    args.test_subdir_name = "validation"
    args.trainval_file = os.path.join(args.input_dir, "ImageSets/Main/trainval.txt")
    args.test_file = os.path.join(args.input_dir, "ImageSets/Main/test.txt")

    dataset_path(args)