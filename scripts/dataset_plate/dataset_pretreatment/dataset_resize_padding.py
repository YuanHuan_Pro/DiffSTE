import argparse
from PIL import Image
import os
import sys
from tqdm import tqdm

sys.path.insert(0, '/yuanhuan/code/demo/Image/sd/diffste')
from scripts.dataset_plate.utils.folder_tools import *


def dataset_resize_padding(args):
    
    # mkdir
    create_folder(args.output_dir)

    # img list 
    img_list = get_sub_filepaths_suffix(args.input_dir, '.jpg')
    print("data len: ", len(img_list))

    for idx in tqdm(range(len(img_list))):

        img_path = img_list[idx]
        img_name = os.path.basename(img_path)
        # output_path = os.path.join(args.output_dir, img_name)
        # original_Texas_665_scale_padding/Texas_style_1/img_name
        output_path = os.path.join(args.output_dir, os.path.basename(os.path.dirname(img_path)), img_name)

        # img
        image = Image.open(img_path)

        # resize
        image_resize = image.resize(args.to_resize_size, Image.BICUBIC)

        # padding
        iw, ih = args.to_resize_size
        w, h = args.to_padding_size
        image_padding = Image.new('RGB', args.to_padding_size, (0, 0, 0))
        image_padding.paste(image_resize, ((w-iw)//2, (h-ih)//2))

        # mkdir
        create_folder(os.path.dirname(output_path))
        image_padding.save(output_path)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser() 
    parser.add_argument('--input_dir', type=str, default="/yuanhuan/data/image/RM_ANPR/original/us/DIFFSTE/original_Texas_665") 
    parser.add_argument('--output_dir', type=str, default="/yuanhuan/data/image/RM_ANPR/original/us/DIFFSTE/original_Texas_665_scale_padding") 
    args = parser.parse_args()

    args.to_resize_size = (256, 128)
    args.to_padding_size = (256, 256)

    dataset_resize_padding(args)

