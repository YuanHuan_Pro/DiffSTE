import argparse
import cv2
import numpy as np
import os
from PIL import Image
import sys
from tqdm import tqdm
import xml.etree.ElementTree as ET

sys.path.insert(0, '/yuanhuan/code/demo/Image/sd/diffste')
from scripts.dataset_plate.utils.folder_tools import *


def read_xml_value(root, name, length):
    vars = root.findall(name)
    if len(vars) == 0:
        raise NotImplementedError('Can not find %s in %s.' % (name, root.tag))
    if length > 0 and len(vars) != length:
        raise NotImplementedError('The size of %s is supposed to be %d, but is %d.' % (name, length, len(vars)))
    if length == 1:
        vars = vars[0]
    return vars


def read_xml(xml_file):
    
    # 载入xml
    tree = ET.parse(xml_file)
    root = tree.getroot()

    points = []
    # 处理box
    for obj in root.findall('object'):
        char_ = read_xml_value(obj, 'name', 1).text
        bndbox = read_xml_value(obj, 'bndbox', 1)
        xmin = float(read_xml_value(bndbox, 'xmin', 1).text)
        ymin = float(read_xml_value(bndbox, 'ymin', 1).text)
        xmax = float(read_xml_value(bndbox, 'xmax', 1).text)
        ymax = float(read_xml_value(bndbox, 'ymax', 1).text)
        assert (xmax > xmin), "xmax <= xmin, {}".format(xml_file)
        assert (ymax > ymin), "ymax <= ymin, {}".format(xml_file)

        points.append((char_, [xmin, ymin, xmax, ymin, xmax, ymax, xmin, ymax]))

    return points


def dataset_mask(args):

    for idx in range(len(args.subdir_list)):

        subdir_idx = args.subdir_list[idx]
        img_dir = os.path.join(args.input_dir, subdir_idx, args.img_subdir_name)
        ann_dir = os.path.join(args.input_dir, subdir_idx, args.xml_subdir_name)
        output_mask_dir = os.path.join(args.input_dir, subdir_idx, args.maks_subdir_name)
        output_mask_txt_path = os.path.join(args.input_dir, subdir_idx, "mask.txt")
        os.makedirs(output_mask_dir, exist_ok=True)

        # img list 
        img_list = os.listdir(img_dir)

        with open(output_mask_txt_path, 'w', encoding='gbk') as f:

            for img_idx in tqdm(range(len(img_list))):

                img_path = os.path.join(img_dir, img_list[img_idx])
                ann_path = os.path.join(ann_dir, img_list[img_idx].replace('.jpg', '.xml'))

                # img
                img = Image.open(img_path)
                img = np.asarray(img)

                # bbox
                boxes = read_xml(ann_path)
                boxes = sorted(boxes, key=lambda x: x[1][0])  # 重新排序，按照每个位置

                mask_info = [img_path]
                for bbox_idx in range(len(boxes)):
                    roi_corners = np.array(boxes[bbox_idx][1], dtype=np.int32).reshape(4, 2)
                    mask = 255 * np.zeros(img.shape[:2], dtype=np.uint8)
                    cv2.fillPoly(mask, [roi_corners], color=(255, 255, 255))
                    
                    mask_path = os.path.join(output_mask_dir, img_list[img_idx].replace('.jpg', '_mask_{}_{}.jpg'.format(boxes[bbox_idx][0], bbox_idx)))
                    mask = Image.fromarray(np.uint8(mask))
                    mask.save(mask_path)
                    mask_info.append(","+mask_path)

                f.writelines(mask_info)
                f.writelines('\n')
        
        if args.syn_split_bool_list[idx]:

            with open(output_mask_txt_path, 'r', encoding='gbk') as f:

                lines = f.readlines()
                lines_split_len = int(len(lines) / args.syn_split_num) + 1
                
                for syn_split_idx in range(args.syn_split_num):

                    lines_split = lines[syn_split_idx*lines_split_len : (syn_split_idx+1)*lines_split_len]

                    output_mask_split_txt_path = os.path.join(args.input_dir, subdir_idx, "test", "mask_{}.txt".format(syn_split_idx))
                    os.makedirs(os.path.dirname(output_mask_split_txt_path), exist_ok=True)

                    with open(output_mask_split_txt_path, 'w', encoding='gbk') as f:
                        for line in lines_split:
                            f.writelines(line.strip())
                            f.writelines('\n')
    

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_dir', type=str, default="/yuanhuan/data/image/RM_ANPR/training/DIFFSTE/us/original_Texas_665") 
    args = parser.parse_args()

    print("data mask.")

    args.img_subdir_name = "images"
    args.xml_subdir_name = "annotations"
    args.maks_subdir_name = "masks"
    args.subdir_list = ["train", "validation"]
    args.syn_split_bool_list = [True, False]

    # syn split
    args.syn_split_num = 6

    dataset_mask(args)