import argparse
import cv2
import os
import sys
import shutil
from tqdm import tqdm
import xml.etree.ElementTree as ET

# sys.path.insert(0, '/home/huanyuan/code/demo')
sys.path.insert(0, '/yuanhuan/code/demo')
from Image.Basic.utils.folder_tools import *
from Image.Basic.script.xml.xml_write import write_xml


def dataset_xml_char_refine(args):

    # jpg list 
    jpg_list = get_sub_filepaths_suffix(args.input_dir, '.jpg')
    print("data len: ", len(jpg_list))

    for idx in tqdm(range(len(jpg_list))):
    
        img_path = jpg_list[idx]
        xml_path = os.path.join(os.path.dirname(img_path), os.path.basename(img_path).replace('.jpg', '.xml'))
        jpg_name = os.path.basename(img_path).replace('.jpg', '')
        lpr_num_name = jpg_name.split('_')[-1]

        # img
        img = cv2.imread(img_path)

        # xml
        tree = ET.parse(xml_path)  # ET是一个 xml 文件解析库，ET.parse（）打开 xml 文件，parse--"解析"
        root = tree.getroot()   # 获取根节点

        # char list
        char_list = []

        # 标签检测和标签转换
        for object in root.findall('object'):
            # name
            classname = str(object.find('name').text)

            # bbox
            bbox = object.find('bndbox')
            pts = ['xmin', 'ymin', 'xmax', 'ymax']
            bndbox = []
            for i, pt in enumerate(pts):
                cur_pt = int(float(bbox.find(pt).text))
                bndbox.append(cur_pt)
            bndbox[0] = max(0, bndbox[0])
            bndbox[1] = max(0, bndbox[1])
            bndbox[2] = min(img.shape[1], bndbox[2])
            bndbox[3] = min(img.shape[0], bndbox[3])

            char_list.append([classname, bndbox, ((bndbox[0] + bndbox[2])/2)])

        def sort_func(x):
            return x[-1]
        char_list = sorted(char_list, key=sort_func)

        # check
        if len(char_list) != len(lpr_num_name):
            print(img_path)
            raise Exception
        # char_refine_bbox
        char_refine_bboxes = {}
        for idx in range(len(char_list)):
            key = lpr_num_name[idx]
            value = char_list[idx][1]
            if key not in char_refine_bboxes:
                char_refine_bboxes[key] = []
            char_refine_bboxes[key].append(value)
        
        write_xml(xml_path, img_path, char_refine_bboxes, img.shape)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser() 
    parser.add_argument('--input_dir', type=str, default="/yuanhuan/data/image/RM_ANPR/original/us/DIFFSTE/original_Texas_665_scale_padding") 
    args = parser.parse_args()

    dataset_xml_char_refine(args)