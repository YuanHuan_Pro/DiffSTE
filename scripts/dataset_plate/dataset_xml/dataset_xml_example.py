import argparse
import os
import sys
import shutil
from tqdm import tqdm

sys.path.insert(0, '/yuanhuan/code/demo/Image/sd/diffste')
from scripts.dataset_plate.utils.folder_tools import *


def dataset_xml_example(args):

    # img list 
    img_list = get_sub_filepaths_suffix(args.input_dir, '.jpg')
    print("data len: ", len(img_list))

    for idx in tqdm(range(len(img_list))):

        img_path = img_list[idx]
        img_name = os.path.basename(img_path)
        example_xml_name = "example.xml"
        input_xml_path = os.path.join(os.path.dirname(img_path), example_xml_name)
        output_xml_path = os.path.join(os.path.dirname(img_path), img_name.replace(".jpg", ".xml"))
        shutil.copy(input_xml_path, output_xml_path)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser() 
    parser.add_argument('--input_dir', type=str, default="/yuanhuan/data/image/RM_ANPR/original/us/DIFFSTE/original_Texas_665_scale_padding") 
    args = parser.parse_args()

    dataset_xml_example(args)
