## 【diffste 数据准备】
1. 运行脚本 dataset_select/dataset_group.py，数据分组，按照车牌长度和字符分组
2. 挑选少量但风格样式多样的数据，参与模型训练和推理，用于生成风格多样的样本（实习生）
3. 二次筛选，挑选数据保证车牌号码标注正确，否则后续会产生大量错误的生成样本（实习生）
4. 数据预处理，运行脚本 dataset_pretreatment/dataset_resize_padding.py，resize & padding 到模型训练尺寸，车牌统一缩放到（256，128）的大小，同时 padding 车牌到 DIFFSTE 生成模型数据（256，256）大小
5. 数据字符标注
    - 利用 LabelImg 标注模板，标注一张样例模版，标签为“char”，生成 xml 文件改名 example.xml
    - 运行脚本 dataset_xml/dataset_xml_example.py，复制样例模版批量生成标签
    - 人工检修，每个字符应该方框内，且框长宽比尽量保持一致，都是相似大小的长方形（实习生）
    - 运行脚本 dataset_xml/dataset_xml_char_refine.py，根据车牌号和标签顺序修正标签，标签为数字+字母
6. 训练集合划分
    - 运行脚本 dataset_train_test_split/data_train_test_split.py，划分训练集、验证集、测试集
7. 模型训练数据路径适配
    - 运行脚本 dataset_pretreatment/dataset_move.py，搬运数据至模型适配路径


## 【diffste 推理】
1. 准备推理 mask：
    - 运行脚本 dataset_pretreatment/dataset_mask.py，根据标注文件，生成重绘区域 mask，生成对应的每张卡的推理 replace_txt 文件