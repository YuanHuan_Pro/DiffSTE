import argparse
import numpy as np
import os
import sys

from sklearn.model_selection import train_test_split

sys.path.insert(0, '/yuanhuan/code/demo/Image/sd/diffste')
from scripts.dataset_plate.utils.folder_tools import *


def split(args):
    
    # mkdir
    create_folder(os.path.dirname(args.trainval_file))

    img_list = np.array(get_sub_filepaths_suffix(args.input_dir, args.img_suffix))
    img_list = img_list[[os.path.basename(img).endswith(args.img_suffix) for img in img_list]]

    if args.test_size !=0:
        trainval_list, test_list = train_test_split(img_list, test_size=args.test_size, random_state=0)
    else:
        trainval_list = img_list
        test_list = []

    if args.val_size != 0:
        train_list, val_list = train_test_split(trainval_list, test_size=args.val_size, random_state=0)
    else:
        train_list = img_list
        val_list = []

    print("length: trainval: {}, train: {}, val: {}, test: {}, all: {}".format(len(trainval_list), len(train_list), len(val_list), len(test_list), (len(train_list) + len(val_list) + len(test_list))))
    with open(args.trainval_file, "w") as f:
        for img_path in trainval_list:
            f.write('{}'.format(img_path))
            f.write("\n")

    with open(args.test_file, "w") as f:
        for img_path in test_list:
            f.write('{}'.format(img_path))
            f.write("\n")

    with open(args.train_file, "w") as f:
        for img_path in train_list:
            f.write('{}'.format(img_path))
            f.write("\n")

    with open(args.val_file, "w") as f:
        for img_path in val_list:
            f.write('{}'.format(img_path))
            f.write("\n")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--input_dir', type=str, default="/yuanhuan/data/image/RM_ANPR/original/us/DIFFSTE/original_Texas_665_scale_padding") 
    args = parser.parse_args()

    args.img_suffix = ".jpg"

    print("data train test split.")

    args.trainval_file = os.path.join(args.input_dir, "ImageSets/Main/trainval.txt")
    args.train_file = os.path.join(args.input_dir, "ImageSets/Main/train.txt")
    args.val_file = os.path.join(args.input_dir, "ImageSets/Main/val.txt")
    args.test_file = os.path.join(args.input_dir, "ImageSets/Main/test.txt")

    args.test_size = 0.01
    args.val_size = 0.01

    split(args)